/*
Andres Santana, Shawn Weaver
Lexer class
Purpose: given a vector of lexemes, categorize them into valid tokens
*/

#ifndef LEXER_H
#define LEXER_H

#include <cctype>
#include <string>
#include <vector>
#include <set>
#include <iostream>

using std::string;
using std::vector;
using std::set;
using std::cout;

class Lexer {
private:
  vector<string> cleanInput;


  // Lists of elements found in the input
  vector<string> invalidList;
  vector<string> commentList;
	set<int> constList;
	set<string> idList, keyList, specialList;
	set<char> opList, symbolList;

  // Constants used to detect correctness of input
  const char symbols[7] = { ':', ',', '(', ')', '{', '}', ';'};
	const char operators[7] = { '+', '-', '*', '/' , '>', '<', '='};
	const char *keywords[5] = { "int", "if", "then", "else", "endif" };
	
  /* Element testers
  Input: string token
  Return: whether or not token is valid */
	bool addVariable(string);
  bool addConstant(string);
	bool addKeyword(string);

  // Element testers for char tokens
  bool addSymbol(char);
  bool addOperator(char);

  // Comment block detectors
  bool isBlockStart(char o, char t) { return o == '/' && t == '*'; };
  bool isBlockEnd(char o, char t) { return o == '*' && t == '/'; };

  // Line comment detectors
  bool isLineStart(char o, char t) { return o == '/' && o == t; };
  bool isLineEnd(char t) { return t == '\n'; };

public:
  Lexer() {};
	
  void sanitize(vector<string> v);

  // Parse and categorize vector v into valid tokens
	void parse(vector<string> v);

  // Display all lexemes in a given token group
	void display();
};

#endif
