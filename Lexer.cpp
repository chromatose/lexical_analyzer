/*
Andres Santana and Shawn Weaver
Lexer.cpp implementation class
*/
#include "Lexer.h"

// This is a rewritten version of the POSIX function strdup which causes issues when compiling on different platforms.
char *my_strdup(const char *str) {
    size_t len = strlen(str);
    char *x = (char *)malloc(len+1); /* 1 for the null terminator */
    if(!x) return NULL; /* malloc could not allocate memory */
    memcpy(x,str,len+1); /* copy the string into the new buffer */
    return x;
}

bool Lexer::addVariable(string s)
{

  // Variables cannot start with numbers
	if (!isalpha(s[0]))
    return false;


  for (size_t i = 1; i < s.length(); ++i)
  {
    // Variables can have letters, numbers, or [] after the first letter
    if (!isalnum(s[i]) && s[i] != '[' && s[i] != ']')
      return false;

    // Arrays cannot have a non-numerical index
    if (i + 1 < s.length() && s[i] == '[' && !isdigit(s[i + 1]))
      return false;
  }

  idList.insert(s);
  return true;
}

bool Lexer::addConstant(string s)
{
  char *end;
  int convert = strtol(s.c_str(), &end, 10);

  // If end contains something, conversion wasn't successful
  if (strlen(end) != 0)
    return false;

  constList.insert(convert);
  return true;
}

bool Lexer::addSymbol(char c)
{
  // Argument must match one of the symbol constants
  for (char sym : symbols)
  {
    if (c == sym)
    {
      symbolList.insert(c);
      return true;
    }
  }
  return false;
}

bool Lexer::addOperator(char c)
{
  // Argument must match one of the op constants
  for (char op : operators)
  {
    if (c == op)
    {
      opList.insert(c);
      return true;
    }
  }
  return false;
}

bool Lexer::addKeyword(string s)
{
  // Argument must match one of the keyword constants
  for (string key : keywords)
  {
    if (strcmp(s.c_str(), key.c_str()) == 0)
    {
      keyList.insert(s);
      return true;
    }
  }
  return false;
}

void Lexer::sanitize(vector<string> v)
{
  // Remove empty and single-space elements from v
  vector<string>::iterator it;
  for (it = v.begin(); it != v.end(); ++it)
  {
    if (!isspace((*it)[0]) && strlen((*it).c_str()) > 0)
      cleanInput.push_back(*it);
  }
}

void Lexer::parse(vector<string> v)
{
  sanitize(v);

  vector<string>::iterator it;
  for (it = cleanInput.begin(); it != cleanInput.end(); ++it)
  {
    // Stop parsing at file end
    if (strcmp(it->c_str(), "$") == 0)
      break;

    // Handle block comments
    if ((it + 1) != cleanInput.end() && isBlockStart((*it)[0], (*(it + 1))[0]))
    {
      // If one was found, increment by two characters /* which don't go into comment list
      it += 2;
      string comment("");
      // Add all comments - even ones in different tokens - until end characters */ found
      while ((it + 1) != cleanInput.end() && !isBlockEnd((*it)[0], (*(it + 1))[0]))
      {
        comment += *it;
	      comment += " ";
        ++it;
      }
      commentList.push_back(comment);
    }

    // Add operators, symbols, and negative number constants
    if (it->length() == 1)
    {
      if (addOperator((*it)[0]) || addSymbol((*it)[0]))
      {
        // Add negative number and increment iterator so it doesn't get added twice
        if ((*it)[0] == '-' && it + 1 != cleanInput.end())
        {
          string negativeNum = "-";
          negativeNum += *(it + 1);
          addConstant(negativeNum);
          it++;
        }
        continue;
      }
    }

    // Add other constants
    if (addConstant((*it)))
      continue;

    // Tokenize by space if not a comment
    string tokenize = *it;
    char* wtokenize = my_strdup(tokenize.c_str());
    char* tok = strtok(wtokenize, " ");
    bool found = false;
    while (tok != NULL)
    {
      // Add keywords and variables
      if (addKeyword(tok) || addVariable(tok) || addOperator(tok[0]))
        found = true;
      else
        invalidList.push_back(tok);
      tok = strtok(NULL, " ");
    }
    free(wtokenize);
    if (found)
      continue;
    
  }
}

void Lexer::display()
{
  /*for (string s : input)
    cout << s << "\n";*/

  // Display list of identifiers
  cout << "Variables or identifiers: ";
  for (string s : idList)
  {
    cout << s << " ";
  }
  cout << "\n";

  // Display invalid identifiers if they exist
  if (invalidList.size() > 0)
  {
    cout << "Invalid identifiers: ";
    for (string s : invalidList)
    {
      cout << s << " ";
    }
    cout << "\n";
  }

  // Display list of operators
  cout << "Operators: ";
  for (char c : opList)
    cout << c << " ";
  cout << "\n";

  // Display list of constants
  cout << "Constants: ";
  for (int i : constList)
    cout << i << " ";
  cout << "\n";

  // Display list of keywords
  cout << "Keywords: ";
  for (string s : keyList)
    cout << s << " ";
  cout << "\n";

  // Display list of symbols
  cout << "Special symbols: ";
  for (char c : symbolList)
    cout << c << " ";
  cout << "\n";
 
  // Display list of comments
  cout << "Comments: ";
  for (string s: commentList)
    cout << s << " ";
	cout << "\n";
}
