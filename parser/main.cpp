#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <vector>

using namespace std;

int main()
{
  string t;
  string space = " ";
  string lastNTerm = "";
  ifstream inFile;
  vector <string> terms;
  vector <string> nterms;
  vector <string> rules;
  vector <string> ilist;
  vector<string>::iterator iter;
  int linec = 0;
  
  inFile.open("input.txt");
  if (!inFile)
    cout << "File read error.\n";

  while (getline(inFile, t))
    {
      vector <string> toks;
      
      rules.push_back(t);
      linec++;
      cout << t << endl;

      char * cstr = strdup(t.c_str());
      char * token = strtok(cstr, " ");
      while (token != NULL)
	{
	  toks.push_back(string(token));
	  token = strtok(NULL, " ");
	}

      if (linec == 1)
	lastNTerm = toks[0];
      
      if (lastNTerm.compare(toks[0]) != 0)
	{
	  nterms.push_back(lastNTerm);
	}
      lastNTerm = toks[0];
    }
  nterms.push_back(lastNTerm);

  for (int i = 0; i < rules.size(); i++)
    {
      string s = rules[i];
      size_t pos = 0;
      pos = s.find(" ", pos);
      pos++;
      while (pos < s.length())
	{
	  string insert = s;
	  pos = insert.find(" ", pos);
	  if (pos != -1)
	    {
	      insert[pos] = '.';
	      ilist.push_back(insert);
	      pos++;
	    }
	}
      string lastInsert = s;
      lastInsert.push_back('.');
      ilist.push_back(lastInsert);
    }
  
  for (int i = 0; i < nterms.size(); i++)
    {
      cout << nterms[i] << endl;
    }

  for (int i = 0; i < ilist.size(); i++)
    {
      cout << ilist[i] << endl;
    }
  
  return 0;
}
