#include <iostream>
#include <iterator>
#include <string.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>

#include "Lexer.h"


int main()
{
  std::ifstream in("input.txt");
  if (!in)
  {
    cout << "There was an error opening the file.\n"
      << "Are you sure you have it in the right folder?\n";
    return 1;
  }
  

  // Setup filestream and convert to string.
  std::string contents((std::istreambuf_iterator<char>(in)),
		       std::istreambuf_iterator<char>());
  
  std::vector<std::string> res;
  size_t pos = 0, lPos = 0;

  /*
    This next section iterates through the entire file and breaks based on delimiters.
    We cannot simply use strtok as we lose the delimiters which are crucial.
    It then pushes to our string vector which is what the Lexer class is designed to use.
   */
  
  while ((pos = contents.find_first_of(";,>*+-/\n=() ", lPos)) != std::string::npos)
    {
      res.push_back(contents.substr(lPos, pos-lPos));
      lPos = pos;
      // This is where we push the delimiter by itself.
      res.push_back(contents.substr(lPos, 1));
      lPos++;

    }
  res.push_back(contents.substr(lPos));

  Lexer lex;
  lex.parse(res);
  lex.display();

  return 0;

}
